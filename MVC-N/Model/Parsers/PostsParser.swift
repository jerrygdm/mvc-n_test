//
//  PostParser.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import Foundation

struct PostsParser : ParserProtocol
{
    func parse(data: AnyObject) -> AnyObject?
    {
        var json: Array<AnyObject>!
        
        var parsedObject = Array<AnyObject>()
        
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data as! NSData, options: NSJSONReadingOptions()) as? Array
        } catch {
            print(error)
        }
        
        for index in 0..<json.count
        {
            if let singlePost = json[index] as? Dictionary<String, AnyObject>
            {
                let post = Post()
                post.postId = singlePost["id"] as? Int
                post.userId = singlePost["userId"] as? Int
                post.title = singlePost["title"] as? String
                post.body = singlePost["body"] as? String
                
                parsedObject.append(post)
            }
        }
        
        return parsedObject
    }
}