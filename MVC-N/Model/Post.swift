//
//  Post.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import Foundation

class Post : AnyObject
{
    var postId : Int?
    var userId : Int?
    var title : String?
    var body : String?
}