//
//  ViewController.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NetworkControllerProtocol
{
    var networkManager: NetworkManagerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.networkManager?.requestData() {
            print("success")
            return true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

