//
//  NetworkControllerProtocol.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import Foundation

protocol NetworkControllerProtocol
{
    var networkManager : NetworkManagerProtocol? { get set }
}