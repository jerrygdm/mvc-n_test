//
//  KaiserBlogRequest.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import Foundation

class ExampleGetRequest: MyNetworkRequest
{
    private let url = "http://jsonplaceholder.typicode.com/posts"
    
    init()
    {
        super.init(url: url)
    }
    
    override func processData()
    {
//        let dataString = NSString(data: incomingData, encoding: NSUTF8StringEncoding)
        
        let parser = PostsParser()
        let arr = parser.parse(incomingData)
        
        print(arr)

    }
}