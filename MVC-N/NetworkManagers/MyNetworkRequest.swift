//
//  MyNetworkRequest.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import Foundation

class MyNetworkRequest: NSOperation
{    
    internal var incomingData = NSMutableData()
    private var sessionTask: NSURLSessionTask?
    private var urlString : String?
    
    var localURLSession: NSURLSession
    {
        return NSURLSession(configuration: localConfig, delegate: self, delegateQueue: nil)
    }
    
    var localConfig: NSURLSessionConfiguration
    {
        return NSURLSessionConfiguration.defaultSessionConfiguration()
    }
    
    init(url urlString: String)
    {
        self.urlString = urlString
        super.init()
    }
    
    var internalFinished = false
    override var finished: Bool {
        get {
            return internalFinished
        }
        set (newValue) {
            willChangeValueForKey("isFinished")
            internalFinished = newValue
            didChangeValueForKey("isFinished")
        }
    }
    
    override func start()
    {
        if cancelled
        {
            finished = true
            return
        }
        
        guard let url = NSURL(string: urlString!) else { fatalError("Failed to build URL") }
        
        let request = NSMutableURLRequest(URL: url)
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        sessionTask = localURLSession.dataTaskWithRequest(request)
        sessionTask!.resume()
    }
    
    func processData()
    {
        
    }
}

extension MyNetworkRequest : NSURLSessionDelegate
{
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void)
    {
        if cancelled
        {
            finished = true
            sessionTask?.cancel()
            return
        }
        
        completionHandler(.Allow)
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData)
    {
        if cancelled
        {
            finished = true
            sessionTask?.cancel()
            return
        }
        
        incomingData.appendData(data)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?)
    {
        if cancelled
        {
            finished = true
            sessionTask?.cancel()
            return
        }
        
        if NSThread.isMainThread() { print("Main Thread!") }
        if error != nil
        {
            print("Failed to receive response: \(error)")
            finished = true
            return
        }
        
        processData()
        finished = true
    }
}