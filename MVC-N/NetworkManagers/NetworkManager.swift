//
//  NetworkManager.swift
//  MVC-N
//
//  Created by Gianmaria Dal Maistro on 24/07/16.
//  Copyright © 2016 Whiteworld. All rights reserved.
//

import UIKit

class NetworkManager: NSObject, NetworkManagerProtocol
{
    let queue = NSOperationQueue()
    
    func requestData()
    {
        let request = ExampleGetRequest()
        queue.addOperation(request)
    }

    func requestData(completion: (Void) -> Bool)
    {
        let request = ExampleGetRequest()
        queue.addOperation(request)

        completion()
    }
}
